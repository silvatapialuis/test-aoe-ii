import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IndexComponent } from './components/index/index.component';
import { CivilizacionesComponent } from './components/civilizationes/civilizaciones.component'
import { UnitsComponent } from './components/units/units.component';
import { StructuresComponent } from './components/structures/structures.component';
import { TechnologiesComponent } from './components/technologies/technologies.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/index',
    pathMatch: 'full'
  },
  {
    path: 'index',
    component: IndexComponent
  },
  {
    path: 'index/civilizations',
    component: CivilizacionesComponent
  },
  {
    path: 'index/units',
    component: UnitsComponent
  },
  {
    path: 'index/structures',
    component: StructuresComponent
  },
  {
    path: 'index/technologies',
    component: TechnologiesComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
