import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

//Material
import { MatTableModule, MatIconModule, MatButtonModule, MatDialogModule, MatInputModule} from '@angular/material';
import { MatFormFieldModule } from '@angular/material/form-field';

//Routing
import { AppRoutingModule } from './app-routing.module';

//Components
import { AppComponent } from './app.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { IndexComponent } from './components/index/index.component';
import { GameService } from './services/game.service';
import { UnitsComponent } from './components/units/units.component';
import { StructuresComponent } from './components/structures/structures.component';
import { TechnologiesComponent } from './components/technologies/technologies.component';
import { CivilizacionesComponent } from './components/civilizationes/civilizaciones.component';
import { DevelopsInComponent } from './components/develops-in/develops-in.component';
import { UniqueUnitComponent } from './components/unique-unit/unique-unit.component';
import { FilterComponent } from './components/filter/filter.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    IndexComponent,
    UnitsComponent,
    StructuresComponent,
    TechnologiesComponent,
    CivilizacionesComponent,
    DevelopsInComponent,
    UniqueUnitComponent,
    FilterComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MatTableModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
  ],
  providers: [
    GameService
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    DevelopsInComponent,
    UniqueUnitComponent
  ]
})
export class AppModule { }
