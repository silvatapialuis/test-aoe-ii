import { Component, OnInit, Input } from '@angular/core';
import { GameService } from '../../services/game.service';
import { Civilizaciones } from '../models/civilizations.model';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material';
import { UniqueUnitComponent } from '../unique-unit/unique-unit.component';

@Component({
  selector: 'app-civilizaciones',
  templateUrl: './civilizaciones.component.html',
  styleUrls: ['./civilizaciones.component.css']
})

export class CivilizacionesComponent implements OnInit {
 
  public civilizaciones: Civilizaciones[] = [];
  public civ: Civilizaciones;
  public displayedColumns: string[] = [
    'name', 
    'expansion', 
    'army_type',
    'unique_unit',
    'team_bonus', 
    'civilization_bonus'
  ];
  public dataSource : any;

  constructor(private gameService: GameService, public dialog: MatDialog) { }

  ngOnInit() {
    this.gameService.getCivilizations().subscribe(
      res => {
        let x;
        //console.log(res);
        
        x=res['civilizations'];

        x.forEach(element => {
          this.civ = new Civilizaciones();
          this.civ.name = element['name'];
          this.civ.expansion = element['expansion'];
          this.civ.army_type = element['army_type'];
          this.civ.unique_unit = element['unique_unit'];
          this.civ.team_bonus = element['team_bonus'];
          this.civ.civilization_bonus=element['civilization_bonus'];
          this.civilizaciones.push(this.civ);
        });
        //carga de datos en la grilla
        this.dataSource = new MatTableDataSource();
        this.dataSource.data = this.civilizaciones;
     },
      err => console.error(err)
    );
  }
  openDialog(link:any): void {
    
    const dialogRef = this.dialog.open(UniqueUnitComponent, {
      width: '950px', height:'500px',
      data: {url:link[0]}
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  } 

  /*
   applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLocaleLowerCase();
  }
  */
  
}
