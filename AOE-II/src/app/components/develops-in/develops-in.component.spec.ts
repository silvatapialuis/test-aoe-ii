import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DevelopsInComponent } from './develops-in.component';

describe('DevelopsInComponent', () => {
  let component: DevelopsInComponent;
  let fixture: ComponentFixture<DevelopsInComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevelopsInComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DevelopsInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
