import { Component, OnInit, Inject } from '@angular/core';
import { Technologies } from '../models/technologies.model';
import { MatTableDataSource, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { GameService } from 'src/app/services/game.service';


@Component({
  selector: 'app-develops-in',
  templateUrl: './develops-in.component.html',
  styleUrls: ['./develops-in.component.css']
})
export class DevelopsInComponent implements OnInit {

  public technologies: Technologies[] = [];
  public tech: Technologies;

  displayedColumns: string[] = [
    'name',
    'expansion',
    'age',
    'cost',
    'build_time',
    //'reload_time',
    'hit_points',
    //'line_of_sight',
    //'range',
    //'attack',
    'armor',
    'special'
  ];
  dataSource: any;

  constructor(
    private gameService: GameService, 
    public dialogRef: MatDialogRef<DevelopsInComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: any
  ) { }

  ngOnInit() {
    this.gameService.fetch(this.data.url)
    .subscribe((res)=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data =res;
    })
  }
}
