import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {
  //@Input('data') 
   
  @Output() parametro = new EventEmitter<string>();
  auxiliar:string="";

  ngOnInit() {
  }

  constructor(
  ) { }

  applyFilter(filterValue: string) {    
    this.parametro.emit(filterValue);   
  }
}
