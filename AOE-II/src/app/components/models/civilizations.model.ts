import { ListCost } from './list-cost.model';

export class Civilizaciones {
    id: number;
    name: string;
    expansion: string;
    army_type: string;
    unique_unit: string;
    unique_tech: string;
    team_bonus: string;
    civilization_bonus: string;
    description?: string;
    age?: string;
    cost?: ListCost;
    build_time?: number;
    reload_time?: number;
    movement_rate?: number;
    line_of_sight?: number;
    hit_points?: number;
    attack?: number;
    armor?: string;
    attack_bonus?: string;
}