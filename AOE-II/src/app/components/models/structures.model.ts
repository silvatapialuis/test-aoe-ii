import { ListCost } from './list-cost.model';

export class Structures {
    id: number;
    name: string;
    expansion: string;
    age: string;
    cost: ListCost;
    build_time: number;
    hit_points: number;
    line_of_sight: number;
    armor: string;
    special: [];
}