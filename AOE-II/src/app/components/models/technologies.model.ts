import { ListCost } from './list-cost.model';

export class Technologies {
    id: number; 
    name: string; 
    description: string; 
    expansion: string;
    age: string;
    develops_in: string;
    cost: ListCost;
    build_time: number;
    applies_to: [];
    special?: [];
    reload_time?: number;
    hit_points?: number;
    line_of_sight?: number;
    range?: number;
    attack?: number;
    armor?: string;
}