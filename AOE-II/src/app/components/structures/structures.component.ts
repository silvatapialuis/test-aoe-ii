import { Component, OnInit } from '@angular/core';
import { GameService } from '../../services/game.service';
import { Structures } from '../models/structures.model';
import { MatTableDataSource } from '@angular/material/table';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-structures',
  templateUrl: './structures.component.html',
  styleUrls: ['./structures.component.css']
})
export class StructuresComponent implements OnInit {

  private structures: Structures[] = [];
  private struc: Structures;
  public displayedColumns: string[] = [
    'name', 
    'expansion', 
    'age', 
    'cost', 
    'build_time',
    'hit_points', 
    'line_of_sight',
    'armor', 
    'special'
  ];
  private dataSource : any;

  constructor(private gameService: GameService) { }

  ngOnInit() {
    this.gameService.getStructures().subscribe(
      res => {
        let x;
        x=res['structures'];

        x.forEach(element => {
          this.struc = new Structures();
          this.struc.name = element['name'];
          this.struc.expansion = element['expansion'];
          this.struc.age = element['age'];
          this.struc.cost = element['cost'];
          this.struc.build_time = element['build_time'];
          this.struc.hit_points = element['hit_points'];
          this.struc.line_of_sight = element['line_of_sight'];
          this.struc.armor = element['armor'];
          this.struc.special = element['special'];
          this.structures.push(this.struc);
        });
        this.dataSource = new MatTableDataSource();
        this.dataSource.data = this.structures;
      },
      err => console.error(err)
    );
  }
  
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLocaleLowerCase();
  }

}
