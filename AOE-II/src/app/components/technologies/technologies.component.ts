import { Component, OnInit } from '@angular/core';
import { GameService } from '../../services/game.service';
import { Technologies } from '../models/technologies.model';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material';
import { DevelopsInComponent } from '../develops-in/develops-in.component';

@Component({
  selector: 'app-technologies',
  templateUrl: './technologies.component.html',
  styleUrls: ['./technologies.component.css']
})
export class TechnologiesComponent implements OnInit {

  private technologies: Technologies[] = [];
  private tech: Technologies;
  public displayedColumns: string[] = [
    'name', 
    'description', 
    'expansion', 
    'age',
    'develops_in',
    'cost', 
    'build_time'
  ];
  public dataSource : any;

  constructor(private gameService: GameService, public dialog: MatDialog) { }

  ngOnInit() {
    this.gameService.getTechnologies().subscribe(
      res => {
        let x;
        x=res['technologies'];

        x.forEach(element => {
          this.tech = new Technologies();
          this.tech.name = element['name'];
          this.tech.description = element['description'];
          this.tech.expansion = element['expansion'];
          this.tech.age = element['age'];
          this.tech.cost = element['cost'];
          this.tech.build_time = element['build_time'];
          this.tech.develops_in = element['develops_in'];
          this.technologies.push(this.tech);              
        });
        this.dataSource = new MatTableDataSource();
        this.dataSource.data = this.technologies;
      },
      err => console.error(err)
    );
  }

  openDialog(link:string): void {
    console.log(link);
    const dialogRef = this.dialog.open(DevelopsInComponent, {
      width: '950px', height:'500px',
      data: {url:link}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
  
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLocaleLowerCase();
  }
  
}