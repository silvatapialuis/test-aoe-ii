import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UniqueUnitComponent } from './unique-unit.component';

describe('UniqueUnitComponent', () => {
  let component: UniqueUnitComponent;
  let fixture: ComponentFixture<UniqueUnitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UniqueUnitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UniqueUnitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
