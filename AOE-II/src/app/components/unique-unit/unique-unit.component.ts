import { Component, OnInit, Inject } from '@angular/core';
import { Civilizaciones } from '../models/civilizations.model';
import { MatTableDataSource, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { GameService } from 'src/app/services/game.service';

@Component({
  selector: 'app-unique-unit',
  templateUrl: './unique-unit.component.html',
  styleUrls: ['./unique-unit.component.css']
})
export class UniqueUnitComponent implements OnInit {

  public civilizaciones: Civilizaciones[] = [];
  public civ: Civilizaciones;
  
  public displayedColumns: string[] = [
    'name',
    'description', 
    'expansion', 
    'age', 
    //'cost', 
    //'build_time', 
    //'reload_time', 
    //'movement_rate', 
    //'line_of_sight', 
    //'hit_points', 
    //'attack', 
    //'armor', 
    //'attack_bonus'  
  ];
  dataSource: any;

  constructor(
    private gameService: GameService, 
    public dialogRef: MatDialogRef<UniqueUnitComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: any
  ) { }

  ngOnInit() {
    this.gameService.fetch(this.data.url)
    .subscribe((res)=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data =res;
    })
  }

}
