import { Component, OnInit } from '@angular/core';
import { GameService } from '../../services/game.service';
import { Units } from '../models/units.model';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-units',
  templateUrl: './units.component.html',
  styleUrls: ['./units.component.css']
})
export class UnitsComponent implements OnInit {

  private units: Units[] = [];
  private uni: Units;
  public displayedColumns: string[] = [
    'name', 
    'description', 
    'expansion', 
    'age',
    'cost', 
    'build_time',
    'reload_time', 
    'attack_delay', 
    'movement_rate', 
    'line_of_sight', 
    'hit_points',
    'range', 
    'attack', 
    'armor', 
    'accuracy'
  ];
  public dataSource : any;

  constructor(private gameService: GameService) { }

  ngOnInit() {
    this.gameService.getUnits().subscribe(
      res => {
        let x;
        x=res['units'];

        x.forEach(element => {
          this.uni = new Units();
          this.uni.name = element['name'];
          this.uni.description = element['description'];
          this.uni.expansion = element['expansion'];
          this.uni.age = element['age'];
          this.uni.cost = element['cost'];
          this.uni.build_time = element['build_time'];
          this.uni.reload_time = element['reload_time'];
          this.uni.attack_delay = element['attack_delay'];
          this.uni.movement_rate = element['attack_delay'];
          this.uni.line_of_sight = element['line_of_sight'];
          this.uni.hit_points = element['hit_points'];
          this.uni.range = element['range'];
          this.uni.attack = element['attack'];
          this.uni.armor = element['armor'];
          this.uni.accuracy = element['accuracy'];
          this.units.push(this.uni);
        });
        this.dataSource = new MatTableDataSource();
        this.dataSource.data = this.units;
      },
      err => console.error(err)
    );
  }
  
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLocaleLowerCase();
  }
}
