import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GameService {


  private API = 'api/v1';

  constructor(private http: HttpClient) { }

  public getCivilizations(){
    return this.http.get(`${this.API}/civilizations`)
  }

  public getCivilization(name: string){
    return this.http.get(`${this.API}/civilization/${name}`);
  }
  
  public getUnits(){
    return this.http.get(`${this.API}/units`);
  }

  public getUnit(name: string){
    return this.http.get(`${this.API}/unit/${name}`);
  }

  public getStructures(){
    return this.http.get(`${this.API}/structures`);
  }

  public getStructure(name: string){
    return this.http.get(`${this.API}/structure/${name}`);
  }

  public getTechnologies(){
    return this.http.get(`${this.API}/technologies`);
  }

  public getTechnology(name: string){
    return this.http.get(`${this.API}/technology/${name}`);
  }

  public fetch(url: string):Observable<any>{
    url = url.replace('https://age-of-empires-2-api.herokuapp.com/api/v1', '')
      return this.http.get(`${this.API}`+url);
    }
}